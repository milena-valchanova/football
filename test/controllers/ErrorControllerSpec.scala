package controllers

import fixtures.FakeApplication
import play.api.test.FakeRequest
import play.api.test.Helpers.stubControllerComponents
import play.api.test.Helpers._

class ErrorControllerSpec extends FakeApplication {
  val controller = new ErrorController(stubControllerComponents())
  "error" should {
    "return responce OK and load template successfully" in {
      status(controller.error()(FakeRequest())) mustBe OK
    }
  }
}
