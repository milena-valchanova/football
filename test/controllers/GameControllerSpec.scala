package controllers

import connectors.GamesConnector
import fixtures.FakeApplication
import models.Game
import org.joda.time.DateTime
import org.mockito.Mockito._
import org.scalatest.BeforeAndAfterEach
import org.scalatest.mockito.MockitoSugar
import org.mockito.Matchers._

import scala.concurrent.Future
import play.api.{Logger, MarkerContext}
import play.api.libs.json.{JsValue, Json}
import play.api.libs.ws.WSClient
import play.api.test._
import play.api.test.Helpers._
import services.GamesService
import utils.CustomException

class GameControllerSpec extends FakeApplication with MockitoSugar with BeforeAndAfterEach {

  val mockGamesConnector: GamesConnector = mock[GamesConnector]
  val mockGamesService: GamesService = mock[GamesService]
  val mockLogger: Logger = mock[Logger]

  val controller = new GameController(stubControllerComponents(), mock[WSClient], app.configuration) {
    override val gamesConnector: GamesConnector = mockGamesConnector
    override val gamesService: GamesService = mockGamesService
    override val logger = mockLogger
  }

  override def beforeEach() = {
    super.beforeEach()
    reset(mockGamesConnector)
    reset(mockGamesService)
    reset(mockLogger)
  }

  "index" should {
    "return responce OK and load template successfully" when {
      "data is collected and parsed successfully" in {
        when(
          mockGamesConnector.getFootballEvents
        ).thenReturn(
          Future.successful(Json.obj())
        )

        when(
          mockGamesService.parse(any[JsValue])
        ).thenReturn(
          Seq()
        )
        when(
          mockGamesService.extractGames(any[Seq[Game]], anyInt, any[DateTime], anyBoolean)
        ).thenReturn(
          Seq()
        )

        status(controller.index()(FakeRequest())) mustBe OK
        verifyZeroInteractions(mockLogger)
      }
    }

    "redirect to error page and log error" when {
      "connection fails with unexpected exception" in {
        when(
          mockGamesConnector.getFootballEvents
        ).thenReturn(
          Future.failed(new RuntimeException)
        )

        status(controller.index()(FakeRequest())) mustBe SEE_OTHER
        verify(mockLogger, times(1)).error(anyString)(any[MarkerContext])
      }

      "GameService.parse throws unexpected exception" in {
        when(
          mockGamesConnector.getFootballEvents
        ).thenReturn(
          Future.successful(Json.obj())
        )

        when(
          mockGamesService.parse(any[JsValue])
        ).thenThrow(
          new RuntimeException
        )

        status(controller.index()(FakeRequest())) mustBe SEE_OTHER
        verify(mockLogger, times(1)).error(anyString)(any[MarkerContext])
      }

      "GameService.extractGames throws unexpected exception" in {
        when(
          mockGamesConnector.getFootballEvents
        ).thenReturn(
          Future.successful(Json.obj())
        )

        when(
          mockGamesService.parse(any[JsValue])
        ).thenReturn(
          Seq()
        )
        when(
          mockGamesService.extractGames(any[Seq[Game]], anyInt, any[DateTime], anyBoolean)
        ).thenThrow(
          new RuntimeException
        )

        status(controller.index()(FakeRequest())) mustBe SEE_OTHER
        verify(mockLogger, times(1)).error(anyString)(any[MarkerContext])
      }
    }

    "redirect to error page without logging error" when {
      "connection fails with custom exception" in {
        when(
          mockGamesConnector.getFootballEvents
        ).thenReturn(
          Future.failed(CustomException("Error message"))
        )

        status(controller.index()(FakeRequest())) mustBe SEE_OTHER
        verifyZeroInteractions(mockLogger)
      }

    }
  }
}
