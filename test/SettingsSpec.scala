import fixtures.FakeApplication

class SettingsSpec extends FakeApplication {

  "Events source" must {
    "be available" in {
      app.configuration.get[String]("urls.games.source") mustBe "test.url"
    }
  }

  "Number of games" must {
    "contain recent games" in {
      app.configuration.get[Int]("number.of.games.recent") mustBe 1
    }

    "contain upcoming games" in {
      app.configuration.get[Int]("number.of.games.upcoming") mustBe 1
    }
  }

}