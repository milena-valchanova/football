package fixtures

import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.inject.guice.GuiceApplicationBuilder

trait FakeApplication extends PlaySpec with GuiceOneAppPerSuite {

  override def fakeApplication() = new GuiceApplicationBuilder().configure(
    Map(
      "urls.games.source" -> "test.url",
      "number.of.games.recent" -> 1,
      "number.of.games.upcoming" -> 1
    )
  ).build()

}
