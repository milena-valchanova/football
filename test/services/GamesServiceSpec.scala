package services

import fixtures.FakeApplication
import models.{Game, Team}
import org.joda.time.DateTime
import org.mockito.Matchers.{any, anyString}
import org.mockito.Mockito.{mock, reset}
import org.scalatestplus.play.PlaySpec
import org.scalatest.BeforeAndAfterEach
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import play.api.{Logger, MarkerContext}
import play.api.libs.json.{JsArray, JsValue, Json}
import utils.CustomException


class GamesServiceSpec extends FakeApplication with MockitoSugar with BeforeAndAfterEach {

  val mockLogger: Logger = mock[Logger]
  val service = new GamesService {
    override val logger: Logger = mockLogger
  }

  override def beforeEach() = {
    reset(mockLogger)
  }

  "parse" should {
    "parse successfully data" when {
      "valid json is given" in {
        val json: JsValue = Json.parse("""{"data": [{"sources":[{"source":"xml_soccer","event_start":{"$numberLong":"1380135600000"},"event_name":"West Brom v Arsenal","object":{"AwayTeam_Id":9,"AwayGoals":5,"HomeTeamObject":{"WIKILink":"http://en.wikipedia.org/wiki/West_Bromwich_Albion_F.C.","Name":"West Brom","Country":"England","LastUpdated":{"$numberLong":"1514345649313"},"Stadium":"The Hawthorns","HomePageURL":"http://www.wba.co.uk/","Team_Id":16},"DateTimestamp":{"$numberLong":"1380135600000"},"HomeGoalDetails":"71': Saido Berahino;","Date":"2013-09-25T19:00:00+00:00","HomeGoals":4,"LastUpdated":{"$numberLong":"1481892786122"},"Spectators":"18604","homeForm":[{"AwayTeam_Id":16,"HomeRedCards":0,"AwayFouls":13,"HomeShotsOnTarget":8,"AwayGoals":1,"AwayLineupForward":" Shane Long; Romelu Lukaku;","HomeGoalDetails":"80': Andrew Carroll;28': Gary O´Neil;16': Andrew Carroll;","DateTimestamp":{"$numberLong":"1364655600000"},"HomeTeamObject":{"WIKILink":"http://en.wikipedia.org/wiki/West_Ham_United_F.C.","Name":"West Ham","Country":"England","LastUpdated":{"$numberLong":"1514345649312"},"Stadium":"The Boleyn Ground","HomePageURL":"http://www.whufc.com","Team_Id":43},"Date":"2013-03-30T15:00:00+00:00","HomeTeamYellowCardDetails":"78': Winston Reid;","AwaySubDetails":"88': in Marc-Antoine Fortuné;88': out Graham Dorrans;71': in Peter Odemwingie;71': out Claudio Yacob;71': in Markus Rosenberg;71': out Shane Long;","HomeLineupDefense":" Joey O´Brien; Guy Demel; Winston Reid; James Collins;","HomeFouls":10,"HomeTeamFormation":"4-3-3","AwayShots":17,"HomeShots":14,"HomeGoals":3,"LastUpdated":{"$numberLong":"1481892857961"},"HalfTimeAwayGoals":0,"HomeLineupMidfield":" Gary O´Neil; Mohamed Diame; Kevin Nolan;","HalfTimeHomeGoals":2,"AwayTeamYellowCardDetails":"68': Shane Long;","Spectators":"34948","HomeYellowCards":1,"AwayShotsOnTarget":11,"HomeTeam":"West Ham","LeagueObject":{"Id":1,"Name":"English Premier League","Country":"England","NumberOfMatches":"6657","IsCup":"false","LastUpdated":{"$numberLong":"1514345414770"},"LatestMatch":"2017-12-26T16:00:00+00:00","LatestMatchTimestamp":{"$numberLong":"1514304000000"},"Fixtures":"Yes","Historical_Data":"Yes","Livescore":"Yes"},"HomeSubDetails":"82': in Jack Collison;82': out Mohamed Diame;79': out Guy Demel;79': in George McCartney;69': out Kevin Nolan;69': in Matthew Taylor;","Id":29240,"AwayGoalDetails":"88': penalty Graham Dorrans;","League":"English Premier League","FixtureMatch_Id":285389,"HomeLineupForward":" Matthew Jarvis; Ricardo Vaz Te; Andrew Carroll;","AwayLineupMidfield":" Claudio Yacob; Graham Dorrans; Youssuf Mulumbu; Chris Brunt;","HomeCorners":5,"AwayTeamObject":{"WIKILink":"http://en.wikipedia.org/wiki/West_Bromwich_Albion_F.C.","Name":"West Brom","Country":"England","LastUpdated":{"$numberLong":"1514345649313"},"Stadium":"The Hawthorns","HomePageURL":"http://www.wba.co.uk/","Team_Id":16},"HomeLineupGoalkeeper":" Jussi Jääskeläinen","AwayCorners":3,"AwayTeamRedCardDetails":"90': Youssuf Mulumbu;","AwayTeamFormation":"4-4-2","_id":{"$oid":"5853bf1bef20f166145c2e8c"},"Round":"31","AwayLineupDefense":" Gareth McAuley; Jonas Olsson; Billy Jones; Liam Ridgewell;","AwayYellowCards":1,"AwayRedCards":1,"AwayTeam":"West Brom","AwayLineupGoalkeeper":" Ben Foster","HomeTeam_Id":43},{"AwayTeam_Id":16,"HomeRedCards":0,"AwayFouls":11,"HomeShotsOnTarget":5,"AwayGoals":0,"HomeTeamObject":{"WIKILink":"http://en.wikipedia.org/wiki/Stoke_City_F.C.","Name":"Stoke","Country":"England","LastUpdated":{"$numberLong":"1514345572950"},"Stadium":"Britannia Stadium","HomePageURL":"http://www.stokecityfc.com","Team_Id":14},"DateTimestamp":{"$numberLong":"1363446000000"},"AwayLineupForward":" Romelu Lukaku;","Date":"2013-03-16T15:00:00+00:00","AwaySubDetails":"84': in Marc-Antoine Fortuné;84': out James Morrison;76': out Romelu Lukaku;76': in Shane Long;67': out Jerome Thomas;67': in Peter Odemwingie;","HomeTeamYellowCardDetails":"59': Matthew Etherington;43': Ryan Shotton;","HomeLineupDefense":" Ryan Shawcross; Marc Wilson; Geoff Cameron; Ryan Shotton;","HomeFouls":15,"HomeTeamFormation":"4-4-2","AwayShots":9,"HomeShots":13,"HomeGoals":0,"LastUpdated":{"$numberLong":"1481892857977"},"HalfTimeAwayGoals":0,"HomeLineupMidfield":" Matthew Etherington; Steven Nzonzi; Glenn Whelan; Cameron Jerome;","HalfTimeHomeGoals":0,"AwayTeamYellowCardDetails":"45': James Morrison;34': Chris Brunt;","HomeYellowCards":2,"AwayShotsOnTarget":5,"LeagueObject":{"Id":1,"Name":"English Premier League","Country":"England","NumberOfMatches":"6657","IsCup":"false","LastUpdated":{"$numberLong":"1514345414770"},"LatestMatch":"2017-12-26T16:00:00+00:00","LatestMatchTimestamp":{"$numberLong":"1514304000000"},"Fixtures":"Yes","Historical_Data":"Yes","Livescore":"Yes"},"HomeSubDetails":"76': in Charlie Adam;76': out Cameron Jerome;63': in Kenwyne Jones;63': out Peter Crouch;46': in Dean Whitehead;46': out Glenn Whelan;","HomeTeam":"Stoke","Id":29090,"League":"English Premier League","FixtureMatch_Id":285377,"HomeLineupForward":" Jonathan Walters; Peter Crouch;","AwayLineupMidfield":" Chris Brunt; Youssuf Mulumbu; James Morrison; Claudio Yacob; Jerome Thomas;","HomeCorners":7,"AwayTeamObject":{"WIKILink":"http://en.wikipedia.org/wiki/West_Bromwich_Albion_F.C.","Name":"West Brom","Country":"England","LastUpdated":{"$numberLong":"1514345649313"},"Stadium":"The Hawthorns","HomePageURL":"http://www.wba.co.uk/","Team_Id":16},"HomeLineupGoalkeeper":" Asmir Begovic","AwayCorners":1,"AwayTeamFormation":"4-5-1","Round":"30","_id":{"$oid":"5853bf1bef20f166145c2eb0"},"AwayLineupDefense":" Liam Ridgewell; Jonas Olsson; Gareth McAuley; Billy Jones;","AwayRedCards":0,"AwayYellowCards":2,"AwayLineupGoalkeeper":" Ben Foster","AwayTeam":"West Brom","HomeTeam_Id":14},{"AwayTeam_Id":19,"HomeRedCards":0,"AwayFouls":10,"AwayGoals":1,"HomeShotsOnTarget":6,"HomeTeamObject":{"WIKILink":"http://en.wikipedia.org/wiki/West_Bromwich_Albion_F.C.","Name":"West Brom","Country":"England","LastUpdated":{"$numberLong":"1514345649313"},"Stadium":"The Hawthorns","HomePageURL":"http://www.wba.co.uk/","Team_Id":16},"DateTimestamp":{"$numberLong":"1362841200000"},"AwayLineupForward":" Luke Moore;","HomeGoalDetails":"61':Own Jonathan De Guzman;40': Romelu Lukaku;","Date":"2013-03-09T15:00:00+00:00","AwaySubDetails":"76': out Wayne Routledge;76': in Itay Shechter;68': in Roland Lamah;68': out Pablo Hernandez;61': out Luke Moore;61': in Nathan Dyer;","HomeTeamYellowCardDetails":"41': Billy Jones;","HomeLineupDefense":" Liam Ridgewell; Jonas Olsson; Gareth McAuley; Billy Jones;","HomeFouls":9,"HomeTeamFormation":"4-2-3-1","AwayShots":9,"HomeShots":12,"HomeGoals":2,"LastUpdated":{"$numberLong":"1481892857952"},"HalfTimeAwayGoals":1,"HomeLineupMidfield":" Chris Brunt; James Morrison; Graham Dorrans; Youssuf Mulumbu; Claudio Yacob;","HalfTimeHomeGoals":1,"AwayTeamYellowCardDetails":"89': Nathan Dyer;","HomeYellowCards":1,"AwayShotsOnTarget":4,"LeagueObject":{"Id":1,"Name":"English Premier League","Country":"England","NumberOfMatches":"6657","IsCup":"false","LastUpdated":{"$numberLong":"1514345414770"},"LatestMatch":"2017-12-26T16:00:00+00:00","LatestMatchTimestamp":{"$numberLong":"1514304000000"},"Fixtures":"Yes","Historical_Data":"Yes","Livescore":"Yes"},"HomeSubDetails":"87': in Marc-Antoine Fortuné;87': out James Morrison;87': in Jerome Thomas;87': out Graham Dorrans;81': out Romelu Lukaku;81': in Peter Odemwingie;","HomeTeam":"West Brom","Id":28932,"AwayGoalDetails":"33': Luke Moore;","League":"English Premier League","FixtureMatch_Id":285367,"HomeLineupForward":" Romelu Lukaku;","AwayLineupMidfield":" Jonathan De Guzman; Wayne Routledge; Michu; Pablo Hernandez; Sung-Yueng Ki;","HomeCorners":3,"AwayTeamObject":{"WIKILink":"http://en.wikipedia.org/wiki/Swansea_City_A.F.C.","Name":"Swansea","Country":"England","LastUpdated":{"$numberLong":"1514345668744"},"Stadium":"Liberty Stadium","HomePageURL":"http://www.swanseacity.net","Team_Id":19},"HomeLineupGoalkeeper":" Ben Foster","AwayCorners":3,"AwayTeamFormation":"4-2-3-1","Round":"29","_id":{"$oid":"5853bf1aef20f166145c2e6f"},"AwayLineupDefense":" Ben Davies; Ashley Williams; Garry Monk; Angel Rangel;","AwayRedCards":0,"AwayYellowCards":1,"AwayLineupGoalkeeper":" Michel Vorm","AwayTeam":"Swansea","HomeTeam_Id":16},{"AwayTeam_Id":5,"HomeRedCards":0,"AwayFouls":10,"AwayGoals":2,"HomeShotsOnTarget":9,"HomeTeamObject":{"WIKILink":"http://en.wikipedia.org/wiki/West_Bromwich_Albion_F.C.","Name":"West Brom","Country":"England","LastUpdated":{"$numberLong":"1514345649313"},"Stadium":"The Hawthorns","HomePageURL":"http://www.wba.co.uk/","Team_Id":16},"DateTimestamp":{"$numberLong":"1358616600000"},"AwayLineupForward":" Gabriel Agbonlahor; Christian Benteke;","HomeGoalDetails":"83': Peter Odemwingie;49': Chris Brunt;","Date":"2013-01-19T17:30:00+00:00","AwaySubDetails":"66': out Charles N´Zogbia;66': in Brett Holman;53': in Barry Bannan;53': out Fabian Delph;5': in Eric Lichaj;5': out Nathan Baker;","HomeTeamYellowCardDetails":"62': Graham Dorrans;43': Romelu Lukaku;21': Jonas Olsson;","HomeLineupDefense":" Gareth McAuley; Jonas Olsson; Liam Ridgewell; Billy Jones;","HomeFouls":8,"HomeTeamFormation":"4-2-3-1","AwayShots":11,"HomeShots":25,"HomeGoals":2,"LastUpdated":{"$numberLong":"1481892857985"},"HalfTimeAwayGoals":2,"HomeLineupMidfield":" Graham Dorrans; James Morrison; Peter Odemwingie; Chris Brunt; Claudio Yacob;","HalfTimeHomeGoals":0,"AwayTeamYellowCardDetails":"61': Matthew Lowton;58': Eric Lichaj;21': Gabriel Agbonlahor;","Spectators":"25504","HomeYellowCards":3,"AwayShotsOnTarget":7,"LeagueObject":{"Id":1,"Name":"English Premier League","Country":"England","NumberOfMatches":"6657","IsCup":"false","LastUpdated":{"$numberLong":"1514345414770"},"LatestMatch":"2017-12-26T16:00:00+00:00","LatestMatchTimestamp":{"$numberLong":"1514304000000"},"Fixtures":"Yes","Historical_Data":"Yes","Livescore":"Yes"},"HomeSubDetails":"90': in George Thorne;90': out Chris Brunt;82': in Markus Rosenberg;82': out Graham Dorrans;35': out Claudio Yacob;35': in Jerome Thomas;","HomeTeam":"West Brom","Id":27945,"AwayGoalDetails":"31': Gabriel Agbonlahor;12': Christian Benteke;","League":"English Premier League","FixtureMatch_Id":285311,"HomeLineupForward":" Romelu Lukaku;","AwayLineupMidfield":" Fabian Delph; Charles N´Zogbia; Joseph Bennett; Matthew Lowton; Ashley R. Westwood;","HomeCorners":8,"AwayTeamObject":{"WIKILink":"http://en.wikipedia.org/wiki/Aston_Villa_F.C.","Name":"Aston Villa","Country":"England","LastUpdated":{"$numberLong":"1514345440324"},"Stadium":"Villa Park","HomePageURL":"http://www.avfc.co.uk/9laI7Jw","Team_Id":5},"HomeLineupGoalkeeper":" Ben Foster","AwayCorners":8,"AwayTeamFormation":"3-5-2","Round":"23","_id":{"$oid":"5853e3fcef20f166145cf6c2"},"AwayLineupDefense":" Nathan Baker; Ron Vlaar; Ciaran Clark;","AwayRedCards":0,"AwayYellowCards":3,"AwayLineupGoalkeeper":" Brad Guzan","AwayTeam":"Aston Villa","HomeTeam_Id":16},{"AwayTeam_Id":14,"HomeRedCards":0,"AwayFouls":15,"AwayGoals":1,"HomeShotsOnTarget":11,"HomeTeamObject":{"WIKILink":"http://en.wikipedia.org/wiki/West_Bromwich_Albion_F.C.","Name":"West Brom","Country":"England","LastUpdated":{"$numberLong":"1514345649313"},"Stadium":"The Hawthorns","HomePageURL":"http://www.wba.co.uk/","Team_Id":16},"DateTimestamp":{"$numberLong":"1354374000000"},"AwayLineupForward":" Kenwyne Jones;","Date":"2012-12-01T15:00:00+00:00","AwaySubDetails":"65': in Dean Whitehead;65': out Charlie Adam;60': out Matthew Etherington;60': in Michael Kightly;","HomeLineupDefense":" Steven Reid; Gareth McAuley; Jonas Olsson; Goran Popov;","HomeFouls":8,"HomeTeamFormation":"4-4-1-1","AwayShots":6,"HomeShots":14,"HomeGoals":0,"LastUpdated":{"$numberLong":"1481892857982"},"HalfTimeAwayGoals":0,"HomeLineupMidfield":" Youssuf Mulumbu; Graham Dorrans; Markus Rosenberg; Zoltan Gera; Claudio Yacob;","HalfTimeHomeGoals":0,"AwayTeamYellowCardDetails":"75': Dean Whitehead;75': Dean Whitehead;","Spectators":"24739","HomeYellowCards":0,"AwayShotsOnTarget":4,"LeagueObject":{"Id":1,"Name":"English Premier League","Country":"England","NumberOfMatches":"6657","IsCup":"false","LastUpdated":{"$numberLong":"1514345414770"},"LatestMatch":"2017-12-26T16:00:00+00:00","LatestMatchTimestamp":{"$numberLong":"1514304000000"},"Fixtures":"Yes","Historical_Data":"Yes","Livescore":"Yes"},"HomeSubDetails":"82': out Claudio Yacob;82': in Chris Brunt;64': out Markus Rosenberg;64': in Peter Odemwingie;64': out Zoltan Gera;64': in Romelu Lukaku;","HomeTeam":"West Brom","Id":25435,"AwayGoalDetails":"75': Dean Whitehead;","League":"English Premier League","FixtureMatch_Id":285226,"HomeLineupForward":" Shane Long;","AwayLineupMidfield":" Charlie Adam; Glenn Whelan; Matthew Etherington; Jonathan Walters; Steven Nzonzi;","HomeCorners":5,"AwayTeamObject":{"WIKILink":"http://en.wikipedia.org/wiki/Stoke_City_F.C.","Name":"Stoke","Country":"England","LastUpdated":{"$numberLong":"1514345572950"},"Stadium":"Britannia Stadium","HomePageURL":"http://www.stokecityfc.com","Team_Id":14},"HomeLineupGoalkeeper":" Boaz Myhill","AwayCorners":5,"AwayTeamFormation":"4-5-1","Round":"15","_id":{"$oid":"5853e3fcef20f166145cf6b9"},"AwayLineupDefense":" Ryan Shawcross; Ryan Shotton; Robert Huth; Geoff Cameron;","AwayRedCards":0,"AwayYellowCards":3,"AwayLineupGoalkeeper":" Asmir Begovic","AwayTeam":"Stoke","HomeTeam_Id":16},{"AwayTeam_Id":16,"HomeRedCards":0,"AwayFouls":13,"AwayGoals":2,"HomeShotsOnTarget":6,"HomeTeamObject":{"WIKILink":"http://en.wikipedia.org/wiki/Wigan_Athletic_F.C.","Name":"Wigan","Country":"England","LastUpdated":{"$numberLong":"1514345458886"},"Stadium":"DW Stadium","HomePageURL":"http://www.wiganlatics.co.uk","Team_Id":12},"DateTimestamp":{"$numberLong":"1352559600000"},"AwayLineupForward":" Shane Long;","HomeGoalDetails":"45': Arouna Kone;","Date":"2012-11-10T15:00:00+00:00","AwaySubDetails":"87': in Markus Rosenberg;87': out Romelu Lukaku;69': out James Morrison;69': in Zoltan Gera;69': out Shane Long;69': in Peter Odemwingie;","HomeLineupDefense":" Maynor Figueroa; Ivan Ramis; Gary Caldwell;","HomeFouls":9,"HomeTeamFormation":"3-4-3","AwayShots":7,"HomeShots":14,"HomeGoals":1,"LastUpdated":{"$numberLong":"1481892857984"},"HalfTimeAwayGoals":2,"HomeLineupMidfield":" Jean Beausejour; Emmerson Boyce; James McCarthy; Ben Watson;","HalfTimeHomeGoals":1,"HomeYellowCards":1,"AwayShotsOnTarget":4,"LeagueObject":{"Id":1,"Name":"English Premier League","Country":"England","NumberOfMatches":"6657","IsCup":"false","LastUpdated":{"$numberLong":"1514345414770"},"LatestMatch":"2017-12-26T16:00:00+00:00","LatestMatchTimestamp":{"$numberLong":"1514304000000"},"Fixtures":"Yes","Historical_Data":"Yes","Livescore":"Yes"},"HomeSubDetails":"86': in Mauro Boselli;86': out Franco Di Santo;86': in Jordi Gomez;86': out Ben Watson;65': out Gary Caldwell;65': in Ronnie Stam;","HomeTeam":"Wigan","Id":24920,"AwayGoalDetails":"43':Own Gary Caldwell;31': James Morrison;","League":"English Premier League","FixtureMatch_Id":285188,"HomeLineupForward":" Shaun Maloney; Franco Di Santo; Arouna Kone;","AwayLineupMidfield":" James Morrison; Chris Brunt; Youssuf Mulumbu; Claudio Yacob; Romelu Lukaku;","HomeCorners":5,"AwayTeamObject":{"WIKILink":"http://en.wikipedia.org/wiki/West_Bromwich_Albion_F.C.","Name":"West Brom","Country":"England","LastUpdated":{"$numberLong":"1514345649313"},"Stadium":"The Hawthorns","HomePageURL":"http://www.wba.co.uk/","Team_Id":16},"HomeLineupGoalkeeper":" Ali Al-Habsi","AwayCorners":8,"AwayTeamFormation":"4-2-3-1","Round":"11","_id":{"$oid":"5853e3fcef20f166145cf6bf"},"AwayLineupDefense":" Jonas Olsson; Liam Ridgewell; Billy Jones; Gareth McAuley;","AwayRedCards":0,"AwayYellowCards":4,"AwayLineupGoalkeeper":" Boaz Myhill","AwayTeam":"West Brom","HomeTeam_Id":12}],"LeagueObject":{"Id":35,"Name":"League Cup","Country":"England","NumberOfMatches":"412","IsCup":"false","LastUpdated":{"$numberLong":"1514345414769"},"LatestMatch":"2017-12-20T21:00:00+00:00","LatestMatchTimestamp":{"$numberLong":"1513803600000"},"Fixtures":"Yes","Historical_Data":"Partial","Livescore":"Yes"},"HomeTeam":"West Brom","AwayGoalDetails":"61': ;","Id":63081,"League":"League Cup","FixtureMatch_Id":314806,"AwayTeamObject":{"WIKILink":"http://en.wikipedia.org/wiki/Arsenal_F.C.","Name":"Arsenal","Country":"England","LastUpdated":{"$numberLong":"1514345649312"},"Stadium":"Emirates Stadium","HomePageURL":"http://www.arsenal.com","Team_Id":9},"awayForm":[{"AwayTeam_Id":9,"HomeRedCards":0,"AwayFouls":6,"AwayGoals":1,"HomeShotsOnTarget":6,"HomeTeamObject":{"WIKILink":"http://en.wikipedia.org/wiki/Queens_Park_Rangers_F.C.","Name":"QPR","Country":"England","LastUpdated":{"$numberLong":"1514345668745"},"Stadium":"Loftus Road","HomePageURL":"http://www.qpr.co.uk/page/Welcome","Team_Id":10},"DateTimestamp":{"$numberLong":"1367685000000"},"AwayLineupForward":" Lukas Podolski;","Date":"2013-05-04T16:30:00+00:00","AwaySubDetails":"90': in Thomas Vermaelen;90': out Tomas Rosicky;89': in Jack Wilshere;89': out Santiago Cazorla;85': out Lukas Podolski;85': in Alex Oxlade-Chamberlain;","HomeTeamYellowCardDetails":"78': Jermaine Jenas;64': Shaun Derry;","HomeLineupDefense":" Armand Traore; Tal Ben Haim; Nedum Onuoha; Clint Hill;","HomeFouls":8,"HomeTeamFormation":"4-4-2","AwayShots":13,"HomeShots":14,"HomeGoals":0,"LastUpdated":{"$numberLong":"1481892857987"},"HalfTimeAwayGoals":1,"HomeLineupMidfield":" Andros Townsend; Stéphane M´Bia; Jermaine Jenas; Ji-Sung Park;","HalfTimeHomeGoals":0,"AwayTeamYellowCardDetails":"59': Nacho Monreal;","Spectators":"18154","HomeYellowCards":2,"AwayShotsOnTarget":11,"LeagueObject":{"Id":1,"Name":"English Premier League","Country":"England","NumberOfMatches":"6657","IsCup":"false","LastUpdated":{"$numberLong":"1514345414770"},"LatestMatch":"2017-12-26T16:00:00+00:00","LatestMatchTimestamp":{"$numberLong":"1514304000000"},"Fixtures":"Yes","Historical_Data":"Yes","Livescore":"Yes"},"HomeSubDetails":"90': in Fabio da Silva;90': out Armand Traore;79': in Adel Taarabt;79': out Ji-Sung Park;46': out Stéphane M´Bia;46': in Shaun Derry;","HomeTeam":"QPR","Id":59881,"AwayGoalDetails":"1': Theo Walcott;","League":"English Premier League","FixtureMatch_Id":285441,"HomeLineupForward":" Loic Remy; Bobby Zamora;","AwayLineupMidfield":" Theo Walcott; Tomas Rosicky; Santiago Cazorla; Mikel Arteta; Aaron Ramsey;","HomeCorners":3,"AwayTeamObject":{"WIKILink":"http://en.wikipedia.org/wiki/Arsenal_F.C.","Name":"Arsenal","Country":"England","LastUpdated":{"$numberLong":"1514345649312"},"Stadium":"Emirates Stadium","HomePageURL":"http://www.arsenal.com","Team_Id":9},"HomeLineupGoalkeeper":" Robert Green","AwayCorners":7,"AwayTeamFormation":"4-2-3-1","Round":"36","_id":{"$oid":"5853e3fcef20f166145cf6cb"},"AwayLineupDefense":" Per Mertesacker; Nacho Monreal; Bacary Sagna; Laurent Koscielny;","AwayRedCards":0,"AwayYellowCards":1,"AwayLineupGoalkeeper":" Wojciech Szczesny","AwayTeam":"Arsenal","HomeTeam_Id":10},{"AwayTeam_Id":5,"HomeRedCards":0,"AwayFouls":12,"AwayGoals":1,"HomeShotsOnTarget":14,"HomeTeamObject":{"WIKILink":"http://en.wikipedia.org/wiki/Arsenal_F.C.","Name":"Arsenal","Country":"England","LastUpdated":{"$numberLong":"1514345649312"},"Stadium":"Emirates Stadium","HomePageURL":"http://www.arsenal.com","Team_Id":9},"DateTimestamp":{"$numberLong":"1361631600000"},"AwayLineupForward":" Gabriel Agbonlahor; Christian Benteke; Andreas Weimann;","HomeGoalDetails":"85': Santiago Cazorla;6': Santiago Cazorla;","Date":"2013-02-23T15:00:00+00:00","AwaySubDetails":"82': out Charles N´Zogbia;82': in Simon Dawkins;73': in Karim El Ahmadi;73': out Ashley R. Westwood;","HomeTeamYellowCardDetails":"38': Abou Diaby;","HomeLineupDefense":" Carl Jenkinson; Per Mertesacker; Thomas Vermaelen; Nacho Monreal;","HomeFouls":7,"HomeTeamFormation":"4-2-3-1","AwayShots":5,"HomeShots":24,"HomeGoals":2,"LastUpdated":{"$numberLong":"1481892857981"},"HalfTimeAwayGoals":0,"HomeLineupMidfield":" Jack Wilshere; Abou Diaby; Mikel Arteta; Theo Walcott; Santiago Cazorla;","HalfTimeHomeGoals":1,"AwayTeamYellowCardDetails":"67': Fabian Delph;37': Matthew Lowton;","Spectators":"59915","HomeYellowCards":1,"AwayShotsOnTarget":3,"LeagueObject":{"Id":1,"Name":"English Premier League","Country":"England","NumberOfMatches":"6657","IsCup":"false","LastUpdated":{"$numberLong":"1514345414770"},"LatestMatch":"2017-12-26T16:00:00+00:00","LatestMatchTimestamp":{"$numberLong":"1514304000000"},"Fixtures":"Yes","Historical_Data":"Yes","Livescore":"Yes"},"HomeSubDetails":"90': in Laurent Koscielny;90': out Theo Walcott;76': in Lukas Podolski;76': out Carl Jenkinson;61': out Abou Diaby;61': in Aaron Ramsey;","HomeTeam":"Arsenal","Id":28571,"AwayGoalDetails":"68': Andreas Weimann;","League":"English Premier League","FixtureMatch_Id":285353,"HomeLineupForward":" Olivier Giroud;","AwayLineupMidfield":" Fabian Delph; Ashley R. Westwood; Charles N´Zogbia;","HomeCorners":12,"AwayTeamObject":{"WIKILink":"http://en.wikipedia.org/wiki/Aston_Villa_F.C.","Name":"Aston Villa","Country":"England","LastUpdated":{"$numberLong":"1514345440324"},"Stadium":"Villa Park","HomePageURL":"http://www.avfc.co.uk/9laI7Jw","Team_Id":5},"HomeLineupGoalkeeper":" Wojciech Szczesny","AwayCorners":3,"AwayTeamFormation":"4-3-3","Round":"27","_id":{"$oid":"5853bf1bef20f166145c2ec3"},"AwayLineupDefense":" Matthew Lowton; Nathan Baker; Ciaran Clark; Joseph Bennett;","AwayRedCards":0,"AwayYellowCards":2,"AwayLineupGoalkeeper":" Brad Guzan","AwayTeam":"Aston Villa","HomeTeam_Id":9},{"AwayTeam_Id":6,"HomeRedCards":0,"AwayFouls":11,"AwayGoals":2,"HomeShotsOnTarget":6,"HomeTeamObject":{"WIKILink":"http://en.wikipedia.org/wiki/Arsenal_F.C.","Name":"Arsenal","Country":"England","LastUpdated":{"$numberLong":"1514345649312"},"Stadium":"Emirates Stadium","HomePageURL":"http://www.arsenal.com","Team_Id":9},"DateTimestamp":{"$numberLong":"1359575100000"},"AwayLineupForward":" Luis Suarez; Daniel Sturridge; Stewart Downing;","HomeGoalDetails":"67': Theo Walcott;65': Olivier Giroud;","Date":"2013-01-30T19:45:00+00:00","AwaySubDetails":"71': out Daniel Sturridge;71': in Jose Enrique;","HomeTeamYellowCardDetails":"90': Per Mertesacker;85': Olivier Giroud;26': Santiago Cazorla;","HomeLineupDefense":" Per Mertesacker; Thomas Vermaelen; Kieran Gibbs; Bacary Sagna;","HomeFouls":9,"HomeTeamFormation":"4-2-3-1","AwayShots":10,"HomeShots":14,"HomeGoals":2,"LastUpdated":{"$numberLong":"1481892857952"},"HalfTimeAwayGoals":1,"HomeLineupMidfield":" Santiago Cazorla; Theo Walcott; Lukas Podolski; Jack Wilshere; Aaron Ramsey;","HalfTimeHomeGoals":0,"AwayTeamYellowCardDetails":"74': Steven Gerrard;","Spectators":"60085","HomeYellowCards":3,"AwayShotsOnTarget":5,"LeagueObject":{"Id":1,"Name":"English Premier League","Country":"England","NumberOfMatches":"6657","IsCup":"false","LastUpdated":{"$numberLong":"1514345414770"},"LatestMatch":"2017-12-26T16:00:00+00:00","LatestMatchTimestamp":{"$numberLong":"1514304000000"},"Fixtures":"Yes","Historical_Data":"Yes","Livescore":"Yes"},"HomeSubDetails":"37': in Andre Santos;37': out Kieran Gibbs;","HomeTeam":"Arsenal","Id":28131,"AwayGoalDetails":"60': Jordan Henderson;5': Luis Suarez;","League":"English Premier League","FixtureMatch_Id":285321,"HomeLineupForward":" Olivier Giroud;","AwayLineupMidfield":" Lucas Leiva; Steven Gerrard; Jordan Henderson;","HomeCorners":8,"AwayTeamObject":{"WIKILink":"http://en.wikipedia.org/wiki/Liverpool_F.C.","Name":"Liverpool","Country":"England","LastUpdated":{"$numberLong":"1514345630190"},"Stadium":"Anfield","HomePageURL":"http://www.liverpoolfc.tv/","Team_Id":6},"HomeLineupGoalkeeper":" Wojciech Szczesny","AwayCorners":6,"AwayTeamFormation":"4-3-3","Round":"24","_id":{"$oid":"5853bf1aef20f166145c2e70"},"AwayLineupDefense":" Andre Wisdom; Glen Johnson; Daniel Agger; Jamie Carragher;","AwayRedCards":0,"AwayYellowCards":1,"AwayLineupGoalkeeper":" Jose Reina","AwayTeam":"Liverpool","HomeTeam_Id":9},{"AwayTeam_Id":125,"AwayGoals":0,"HomeTeamObject":{"WIKILink":"http://en.wikipedia.org/wiki/Arsenal_F.C.","Name":"Arsenal","Country":"England","LastUpdated":{"$numberLong":"1514345649312"},"Stadium":"Emirates Stadium","HomePageURL":"http://www.arsenal.com","Team_Id":9},"HomeGoalDetails":"63': Lukas Podolski;49': Jack Wilshere;","AwayLineupForward":" Gaetan Charbonnier;","DateTimestamp":{"$numberLong":"1353523500000"},"Date":"2012-11-21T18:45:00+00:00","AwaySubDetails":"79': out Marco Estrada;79': in Joris Steve Marveaux;69': in Emmanuel Herrera;69': out Gaetan Charbonnier;69': in Jonas Martin;69': out Remy Cabella;","HomeLineupDefense":" Thomas Vermaelen; Laurent Koscielny; Per Mertesacker; Bacary Sagna;","HomeTeamFormation":"4-2-1-3","HomeGoals":2,"LastUpdated":{"$numberLong":"1481893267853"},"HomeLineupMidfield":" Mikel Arteta; Santiago Cazorla; Jack Wilshere;","LeagueObject":{"Id":16,"Name":"Champions League","Country":"Europe","NumberOfMatches":"925","IsCup":"true","LastUpdated":{"$numberLong":"1514345414776"},"LatestMatch":"2017-12-06T20:45:00+00:00","LatestMatchTimestamp":{"$numberLong":"1512593100000"},"Fixtures":"Yes","Historical_Data":"Partial","Livescore":"Yes"},"HomeTeam":"Arsenal","HomeSubDetails":"85': in Gervinho;85': out Olivier Giroud;84': in Francis Coquelin;84': out Santiago Cazorla;69': in Aaron Ramsey;69': out Alex Oxlade-Chamberlain;","Id":26874,"League":"Champions League","FixtureMatch_Id":290878,"HomeLineupForward":" Lukas Podolski; Olivier Giroud; Alex Oxlade-Chamberlain;","AwayLineupMidfield":" Mapou Yanga-Mbiwa; Anthony Mounier; Younes Belhanda; Remy Cabella; Marco Estrada;","AwayTeamObject":{"WIKILink":"http://en.wikipedia.org/wiki/Montpellier_HSC","Name":"Montpellier","Country":"France","LastUpdated":{"$numberLong":"1514345477361"},"Stadium":"Stade de la Mosson","HomePageURL":"http://www.mhscfoot.com/","Team_Id":125},"HomeLineupGoalkeeper":" Wojciech Szczesny","AwayTeamFormation":"4-2-3-1","_id":{"$oid":"5853c0b7ef20f166145c3543"},"AwayLineupDefense":" Henri Bedimo; Abdelhamid El Kaoutari; Daniel Congré; Mathieu Deplagne;","AwayTeam":"Montpellier","AwayLineupGoalkeeper":" Geoffrey Jourdren","HomeTeam_Id":9},{"AwayTeam_Id":4,"HomeRedCards":0,"AwayFouls":10,"AwayGoals":3,"HomeShotsOnTarget":9,"HomeTeamObject":{"WIKILink":"http://en.wikipedia.org/wiki/Arsenal_F.C.","Name":"Arsenal","Country":"England","LastUpdated":{"$numberLong":"1514345649312"},"Stadium":"Emirates Stadium","HomePageURL":"http://www.arsenal.com","Team_Id":9},"DateTimestamp":{"$numberLong":"1352559600000"},"AwayLineupForward":" Dimitar Berbatov;","HomeGoalDetails":"69': Olivier Giroud;23': Lukas Podolski;11': Olivier Giroud;","Date":"2012-11-10T15:00:00+00:00","AwaySubDetails":"85': in Damien Duff;85': out Ashkan Dejagah;21': in Alex Kacaniklic;21': out Kieran Richardson;","HomeLineupDefense":" Per Mertesacker; Thomas Vermaelen; Bacary Sagna; Laurent Koscielny;","HomeFouls":12,"HomeTeamFormation":"4-2-3-1","AwayShots":9,"HomeShots":15,"HomeGoals":3,"LastUpdated":{"$numberLong":"1481892857985"},"HalfTimeAwayGoals":2,"HomeLineupMidfield":" Santiago Cazorla; Lukas Podolski; Francis Coquelin; Mikel Arteta; Theo Walcott;","HalfTimeHomeGoals":2,"HomeYellowCards":1,"AwayShotsOnTarget":5,"LeagueObject":{"Id":1,"Name":"English Premier League","Country":"England","NumberOfMatches":"6657","IsCup":"false","LastUpdated":{"$numberLong":"1514345414770"},"LatestMatch":"2017-12-26T16:00:00+00:00","LatestMatchTimestamp":{"$numberLong":"1514304000000"},"Fixtures":"Yes","Historical_Data":"Yes","Livescore":"Yes"},"HomeSubDetails":"85': in Andrey Arshavin;85': out Theo Walcott;76': out Lukas Podolski;76': in Alex Oxlade-Chamberlain;56': in Aaron Ramsey;56': out Francis Coquelin;","HomeTeam":"Arsenal","Id":24914,"AwayGoalDetails":"67': penalty Dimitar Berbatov;40': Alex Kacaniklic;29': Dimitar Berbatov;","League":"English Premier League","FixtureMatch_Id":285185,"HomeLineupForward":" Olivier Giroud;","AwayLineupMidfield":" Steve Sidwell; Kieran Richardson; Bryan Ruiz; Ashkan Dejagah; Chris Baird;","HomeCorners":8,"AwayTeamObject":{"WIKILink":"http://en.wikipedia.org/wiki/Fulham_F.C.","Name":"Fulham","Country":"England","LastUpdated":{"$numberLong":"1514345553695"},"Stadium":"Craven Cottage","HomePageURL":"http://www.fulhamfc.com/","Team_Id":4},"HomeLineupGoalkeeper":" Vito Mannone","AwayCorners":6,"AwayTeamFormation":"4-4-1-1","Round":"11","_id":{"$oid":"5853e3fcef20f166145cf6c5"},"AwayLineupDefense":" Aaron Hughes; Brede Hangeland; John Arne Riise; Sascha Riether;","AwayRedCards":0,"AwayYellowCards":2,"AwayLineupGoalkeeper":" Mark Schwarzer","AwayTeam":"Fulham","HomeTeam_Id":9},{"AwayTeam_Id":9,"AwayGoals":2,"HomeTeamObject":{"WIKILink":"http://en.wikipedia.org/wiki/FC_Schalke_04","Name":"Schalke 04","Country":"Germany","LastUpdated":{"$numberLong":"1514345515309"},"Stadium":"Veltins-Arena","HomePageURL":"http://www.schalke04.de/","Team_Id":68},"DateTimestamp":{"$numberLong":"1352227500000"},"HomeGoalDetails":"67': Jefferson Farfan;45': Klaas Jan Huntelaar;","AwayLineupForward":" Olivier Giroud;","Date":"2012-11-06T18:45:00+00:00","AwaySubDetails":"90': in Andre Santos;90': out Lukas Podolski;90': in Francis Coquelin;90': out Santiago Cazorla;","HomeLineupDefense":" Benedikt Höwedes; Joel Matip; Christian Fuchs; Atsuto Uchida;","HomeTeamFormation":"4-2-3-1","HomeGoals":2,"LastUpdated":{"$numberLong":"1481893267844"},"HomeLineupMidfield":" Jefferson Farfan; Lewis Holtby; Ibrahim Afellay; Roman Neustädter; Jermaine Jones;","LeagueObject":{"Id":16,"Name":"Champions League","Country":"Europe","NumberOfMatches":"925","IsCup":"true","LastUpdated":{"$numberLong":"1514345414776"},"LatestMatch":"2017-12-06T20:45:00+00:00","LatestMatchTimestamp":{"$numberLong":"1512593100000"},"Fixtures":"Yes","Historical_Data":"Partial","Livescore":"Yes"},"HomeTeam":"Schalke 04","HomeSubDetails":"90': in Tranquillo Barnetta;90': out Lewis Holtby;66': out Marco Höger;66': in Kyriakos Papadopoulos;25': out Atsuto Uchida;25': in Marco Höger;","Id":26850,"AwayGoalDetails":"26': Olivier Giroud;18': Theo Walcott;","League":"Champions League","FixtureMatch_Id":275178,"HomeLineupForward":" Klaas Jan Huntelaar;","AwayLineupMidfield":" Lukas Podolski; Mikel Arteta; Jack Wilshere; Theo Walcott; Santiago Cazorla;","AwayTeamObject":{"WIKILink":"http://en.wikipedia.org/wiki/Arsenal_F.C.","Name":"Arsenal","Country":"England","LastUpdated":{"$numberLong":"1514345649312"},"Stadium":"Emirates Stadium","HomePageURL":"http://www.arsenal.com","Team_Id":9},"HomeLineupGoalkeeper":" Lars Unnerstall","AwayTeamFormation":"4-2-3-1","_id":{"$oid":"5853c0b6ef20f166145c351b"},"AwayLineupDefense":" Thomas Vermaelen; Bacary Sagna; Laurent Koscielny; Per Mertesacker;","AwayTeam":"Arsenal","AwayLineupGoalkeeper":" Vito Mannone","HomeTeam_Id":68}],"_id":{"$oid":"5853bec4ef20f166145c2d7f"},"Round":"3","AwayTeam":"Arsenal","HomeTeam_Id":16},"sport":"Football","id":"5853bec4ef20f166145c2d7f","participant2":"Arsenal","participant1":"West Brom","competition":"League Cup"}],"_id":{"$oid":"5853bec5ef20f166145c2dc6"}}]}""")
        service.parse(json) mustBe Seq(
          Game(
            "West Brom v Arsenal",
            new DateTime(1380135600000L),
            false,
            Team("West Brom", None),
            Team("Arsenal", None),
            Some(4),
            Some(5)
          )
        )
        verifyZeroInteractions(mockLogger)
      }

      "json contains only mandatory fields" in {
        val json = Json.obj(
          "data" -> Json.arr(
            Json.obj(
              "sources" -> Json.arr(
                Json.obj(
                  "source" -> "xml_soccer",
                  "event_start" -> Json.obj(
                    "$numberLong" -> "1380135600000"
                  ),
                  "event_name" -> "West Brom v Arsenal",
                  "participant1" -> "West Brom",
                  "participant2" -> "Arsenal",
                  "object" -> Json.obj()
                )
              )
            )
          )
        )
        service.parse(json) mustBe Seq(
          Game(
            "West Brom v Arsenal",
            new DateTime(1380135600000L),
            false,
            Team("West Brom", None),
            Team("Arsenal", None),
            None,
            None
          )
        )
        verifyZeroInteractions(mockLogger)
      }

      "json contains data for multiple games" in {
        val json = Json.obj(
          "data" -> Json.arr(
            Json.obj(
              "sources" -> Json.arr(
                Json.obj(
                  "source" -> "xml_soccer",
                  "event_start" -> Json.obj(
                    "$numberLong" -> "1380135600000"
                  ),
                  "event_name" -> "West Brom v Arsenal",
                  "participant1" -> "West Brom",
                  "participant2" -> "Arsenal",
                  "object" -> Json.obj()
                )
              )
            ),
            Json.obj(
              "sources" -> Json.arr(
                Json.obj(
                  "source" -> "xml_soccer",
                  "event_start" -> Json.obj(
                    "$numberLong" -> "1380135600000"
                  ),
                  "event_name" -> "West Brom v Arsenal",
                  "participant1" -> "West Brom",
                  "participant2" -> "Arsenal",
                  "object" -> Json.obj()
                )
              )
            )
          )
        )
        service.parse(json) mustBe Seq(
          Game(
            "West Brom v Arsenal",
            new DateTime(1380135600000L),
            false,
            Team("West Brom", None),
            Team("Arsenal", None),
            None,
            None
          ),
          Game(
            "West Brom v Arsenal",
            new DateTime(1380135600000L),
            false,
            Team("West Brom", None),
            Team("Arsenal", None),
            None,
            None
          )
        )
        verifyZeroInteractions(mockLogger)
      }

      "json contains all needed fields" in {
        val json = Json.obj(
          "data" -> Json.arr(
            Json.obj(
              "sources" -> Json.arr(
                Json.obj(
                  "source" -> "xml_soccer",
                  "event_start" -> Json.obj(
                    "$numberLong" -> "1380135600000"
                  ),
                  "event_name" -> "West Brom v Arsenal",
                  "participant1" -> "West Brom",
                  "participant2" -> "Arsenal",
                  "object" -> Json.obj(
                    "HomeLineUpCoach" -> "Alan Pardew",
                    "AwayLineUpCoach" -> "Arsène Wenger",
                    "HomeGoals" -> 4,
                    "AwayGoals" -> 5
                  )
                ),
                Json.obj(
                  "source" -> "live_streaming"
                )
              )
            )
          )
        )
        service.parse(json) mustBe Seq(
          Game(
            "West Brom v Arsenal",
            new DateTime(1380135600000L),
            true,
            Team("West Brom", Some("Alan Pardew")),
            Team("Arsenal", Some("Arsène Wenger")),
            Some(4),
            Some(5)
          )
        )
        verifyZeroInteractions(mockLogger)
      }

      "json doesn't contain object field" in {
        val json = Json.obj(
          "data" -> Json.arr(
            Json.obj(
              "sources" -> Json.arr(
                Json.obj(
                  "source" -> "xml_soccer",
                  "event_start" -> Json.obj(
                    "$numberLong" -> "1380135600000"
                  ),
                  "event_name" -> "West Brom v Arsenal",
                  "participant1" -> "West Brom",
                  "participant2" -> "Arsenal"
                ),
                Json.obj(
                  "source" -> "live_streaming"
                )
              )
            )
          )
        )
        service.parse(json) mustBe Seq(
          Game(
            "West Brom v Arsenal",
            new DateTime(1380135600000L),
            true,
            Team("West Brom", None),
            Team("Arsenal", None),
            None,
            None
          )
        )
        verifyZeroInteractions(mockLogger)
      }
    }

    "throw custom exception" when {
      "data is missing from json" in {
        val json = Json.obj()
        intercept[CustomException] {
          service.parse(json)
        }.getMessage mustBe "Invalid JSON: data doesn't exist."
        verify(mockLogger, times(1)).error(anyString)(any[MarkerContext])
      }

      "xml_soccer is missing from json" in {
        val json = Json.obj(
          "data" -> Json.arr(
            Json.obj(
              "sources" -> Json.arr(
                Json.obj(
                  "source" -> "live_streaming"
                )
              )
            )
          )
        )
        intercept[CustomException] {
          service.parse(json)
        }.getMessage mustBe "Invalid JSON: xml_soccer doesn't exist."
        verify(mockLogger, times(1)).error(anyString)(any[MarkerContext])
      }

      "mandatory fields are missing from json" in {
        val json = Json.obj(
          "data" -> Json.arr(
            Json.obj(
              "sources" -> Json.arr(
                Json.obj(
                  "source" -> "xml_soccer"
                )
              )
            )
          )
        )
        intercept[CustomException] {
          service.parse(json)
        }.getMessage mustBe "Invalid JSON: List((/participant1,List(JsonValidationError(List(error.path.missing),WrappedArray()))), (/event_start/$numberLong,List(JsonValidationError(List(error.path.missing),WrappedArray()))), (/event_name,List(JsonValidationError(List(error.path.missing),WrappedArray()))), (/participant2,List(JsonValidationError(List(error.path.missing),WrappedArray()))))."
        verify(mockLogger, times(1)).error(anyString)(any[MarkerContext])
      }
    }
  }

  "extractGames" should {
    val now = DateTime.now()
    val testGames = Seq(
      Game(
        name = "Game1",
        start = now,
        isLiveStreamed = true,
        homeTeam = Team("Home Team", None),
        awayTeam = Team("Away Team", None),
        homeGoals = None,
        awayGoals = None
      ),
      Game(
        name = "Game2",
        start = now.minusDays(3),
        isLiveStreamed = true,
        homeTeam = Team("Home Team", None),
        awayTeam = Team("Away Team", None),
        homeGoals = None,
        awayGoals = None
      ),
      Game(
        name = "Game3",
        start = now.plusHours(3),
        isLiveStreamed = true,
        homeTeam = Team("Home Team", None),
        awayTeam = Team("Away Team", None),
        homeGoals = None,
        awayGoals = None
      ),
      Game(
        name = "Game4",
        start = now.minusMinutes(3),
        isLiveStreamed = true,
        homeTeam = Team("Home Team", None),
        awayTeam = Team("Away Team", None),
        homeGoals = None,
        awayGoals = None
      ),
      Game(
        name = "Game5",
        start = now.plusMonths(3),
        isLiveStreamed = true,
        homeTeam = Team("Home Team", None),
        awayTeam = Team("Away Team", None),
        homeGoals = None,
        awayGoals = None
      )
    )

    "display list of all recent games" when {
      "it's required to display more data than is in the list" in {
        val result = service.extractGames(games = testGames, numberOfGames = 5, date = now, areUpcoming = false)
        result mustBe Seq(
          Game(
            name = "Game4",
            start = now.minusMinutes(3),
            isLiveStreamed = true,
            homeTeam = Team("Home Team", None),
            awayTeam = Team("Away Team", None),
            homeGoals = None,
            awayGoals = None
          ),
          Game(
            name = "Game2",
            start = now.minusDays(3),
            isLiveStreamed = true,
            homeTeam = Team("Home Team", None),
            awayTeam = Team("Away Team", None),
            homeGoals = None,
            awayGoals = None
          )
        )
      }
    }

    "display the most recent game" when {
      "only one game is required" in {
        val result = service.extractGames(games = testGames, numberOfGames = 1, date = now, areUpcoming = false)
        result mustBe Seq(
          Game(
            name = "Game4",
            start = now.minusMinutes(3),
            isLiveStreamed = true,
            homeTeam = Team("Home Team", None),
            awayTeam = Team("Away Team", None),
            homeGoals = None,
            awayGoals = None
          )
        )
      }
    }

    "display list of all upcoming games" when {
      "it's required to display more data than is in the list" in {
        val result = service.extractGames(games = testGames, numberOfGames = 5, date = now, areUpcoming = true)
        result mustBe Seq(
          Game(
            name = "Game1",
            start = now,
            isLiveStreamed = true,
            homeTeam = Team("Home Team", None),
            awayTeam = Team("Away Team", None),
            homeGoals = None,
            awayGoals = None
          ),
          Game(
            name = "Game3",
            start = now.plusHours(3),
            isLiveStreamed = true,
            homeTeam = Team("Home Team", None),
            awayTeam = Team("Away Team", None),
            homeGoals = None,
            awayGoals = None
          ),
          Game(
            name = "Game5",
            start = now.plusMonths(3),
            isLiveStreamed = true,
            homeTeam = Team("Home Team", None),
            awayTeam = Team("Away Team", None),
            homeGoals = None,
            awayGoals = None
          )
        )
      }
    }

    "display next game" when {
      "only one game is required" in {
        val result = service.extractGames(games = testGames, numberOfGames = 1, date = now, areUpcoming = true)
        result mustBe Seq(
          Game(
            name = "Game1",
            start = now,
            isLiveStreamed = true,
            homeTeam = Team("Home Team", None),
            awayTeam = Team("Away Team", None),
            homeGoals = None,
            awayGoals = None
          )
        )
      }
    }
  }
}
