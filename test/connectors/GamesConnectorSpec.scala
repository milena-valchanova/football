package connectors

import play.api.test.Helpers._
import org.scalatest.BeforeAndAfterEach
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import play.api.libs.ws.{WSClient, WSRequest}
import org.mockito.Matchers._
import play.api.{Logger, MarkerContext}
import play.api.libs.json.{JsValue, Json}
import fixtures.FakeApplication
import utils.CustomException
import scala.concurrent.Future

class GamesConnectorSpec extends FakeApplication with MockitoSugar with BeforeAndAfterEach {

  val mockWSClient = mock[WSClient]
  val mockWSRequest = mock[WSRequest]
  val mockWSResponse = mock[mockWSRequest.Response]
  val mockLogger: Logger = mock[Logger]
  val connector = new GamesConnector(mockWSClient, app.configuration) {
    override val logger: Logger = mockLogger
  }

  override def beforeEach() = {
    super.beforeEach()
    reset(mockWSClient)
    reset(mockWSRequest)
    reset(mockWSResponse)
    reset(mockLogger)
  }

  "getFootballEvents" should {

    "return valid json" when {
      "connection with external resource is successfull" in {
        val testObject: JsValue = Json.obj(
          "test-field" -> "test-value"
        )

        when(
          mockWSResponse.json
        ).thenReturn(
          testObject
        )
        when(
          mockWSRequest.get
        ).thenReturn(
          Future.successful(mockWSResponse)
        )
        when(
          mockWSClient.url(anyString)
        ).thenReturn(
          mockWSRequest
        )

        await(connector.getFootballEvents) mustBe testObject
        verifyZeroInteractions(mockLogger)
      }
    }

    "throw exception and log reason for failure" when {
      "connection to external resource fails" in {
        val exceptionMessage = "Test exception message"
        when(
          mockWSRequest.get
        ).thenReturn(
          Future.failed(new RuntimeException(exceptionMessage))
        )
        when(
          mockWSClient.url(anyString)
        ).thenReturn(
          mockWSRequest
        )

        intercept[CustomException] {
          await(connector.getFootballEvents)
        }.getMessage mustBe exceptionMessage
        verify(mockLogger, times(1)).error(anyString)(any[MarkerContext])
      }
    }
  }
}
