package models

case class Team(name: String, lineUpCoach: Option[String])
