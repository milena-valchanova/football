package models.xmlModels

import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Reads}

case class XMLSoccerObject(homeLineUpCoach: Option[String], awayLineUpCoach: Option[String], homeGoals: Option[Int], awayGoals: Option[Int])
object XMLSoccerObject {
  implicit val XMLSoccerObjectReads: Reads[XMLSoccerObject] = (
    (JsPath \ "HomeLineUpCoach").readNullable[String] and
      (JsPath \ "AwayLineUpCoach").readNullable[String] and
        (JsPath \ "HomeGoals").readNullable[Int] and
          (JsPath \ "AwayGoals").readNullable[Int]
    )(XMLSoccerObject.apply _)
}
