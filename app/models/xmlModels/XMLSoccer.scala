package models.xmlModels

import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Reads}

case class XMLSoccer(eventName: String, eventStart: String, participant1: String, participant2: String, xmlSoccerObject: Option[XMLSoccerObject])
object XMLSoccer {
  implicit val XMLSoccerReads: Reads[XMLSoccer] = (
    (JsPath \ "event_name").read[String] and
      (JsPath \ "event_start" \ "$numberLong").read[String] and
        (JsPath \ "participant1").read[String] and
          (JsPath \ "participant2").read[String] and
            (JsPath \ "object").readNullable[XMLSoccerObject]
    )(XMLSoccer.apply _)
}