package models

import org.joda.time.DateTime

case class Game(
                 name: String,
                 start: DateTime,
                 isLiveStreamed: Boolean,
                 homeTeam: Team,
                 awayTeam: Team,
                 homeGoals: Option[Int],
                 awayGoals: Option[Int]
               )