package services

import models.xmlModels.XMLSoccer
import models.{Game, Team}
import play.api.Logger
import play.api.libs.json._
import utils.CustomException
import org.joda.time.DateTime

class GamesService {

  val logger: Logger = Logger

  private def parseGame(gamesData: JsValue): Game = {
    val sources: Seq[JsValue] = gamesData("sources").as[Seq[JsValue]]
    sources.find(_("source").as[String] == "xml_soccer") match {
      case Some(sd: JsValue) =>
        sd.validate[XMLSoccer] match {
          case JsSuccess(soccerData, _) =>
            Game(
              name = soccerData.eventName,
              start = new DateTime(soccerData.eventStart.toLong),
              isLiveStreamed = sources.exists(_("source").as[String] == "live_streaming"),
              homeTeam = Team(
                name = soccerData.participant1,
                lineUpCoach = soccerData.xmlSoccerObject.flatMap(_.homeLineUpCoach)
              ),
              awayTeam = Team(
                name = soccerData.participant2,
                lineUpCoach = soccerData.xmlSoccerObject.flatMap(_.awayLineUpCoach)
              ),
              homeGoals = soccerData.xmlSoccerObject.flatMap(_.homeGoals),
              awayGoals = soccerData.xmlSoccerObject.flatMap(_.awayGoals)
            )
          case JsError(error) =>
            logger.error(s"Invalid JSON: $error.")
            throw CustomException(s"Invalid JSON: $error.")
        }
      case None =>
        logger.error("Invalid JSON: xml_soccer doesn't exist.")
        throw CustomException("Invalid JSON: xml_soccer doesn't exist.")
    }
  }

  def parse(json: JsValue): Seq[Game] = {
    (json \ "data").asOpt[Seq[JsValue]] match {
      case Some(data) =>
        data.map(parseGame)
      case _ =>
        logger.error("Invalid JSON: data doesn't exist.")
        throw CustomException("Invalid JSON: data doesn't exist.")
    }
  }

  def extractGames(games: Seq[Game], numberOfGames: Int, date: DateTime = DateTime.now, areUpcoming: Boolean = false): Seq[Game] = {
    val sortedGames: Seq[Game] = areUpcoming match {
      case true =>
        games
          .filter(game => game.start.isAfter(date) || game.start.isEqual(date))
          .sortWith((game1, game2) => game1.start.isBefore(game2.start))
      case false =>
        games
          .filter(game => game.start.isBefore(date))
          .sortWith((game1, game2) => game1.start.isAfter(game2.start))
    }
    sortedGames.take(numberOfGames)
  }
}
