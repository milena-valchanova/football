package connectors

import javax.inject.{Inject, Singleton}

import play.api.{Configuration, Logger}
import play.api.libs.json.JsValue
import play.api.libs.ws.WSClient
import utils.CustomException

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

@Singleton
class GamesConnector @Inject()(ws: WSClient, config: Configuration) {

  val logger: Logger = Logger
  lazy val footballGamesURL = config.get[String]("urls.games.source")

  def getFootballEvents: Future[JsValue] = {
    ws.url(footballGamesURL).get().map { data =>
      data.json
    }.recover{
      case ex: Exception =>
        logger.error(s"Unable to retrieve data: ${ex.getMessage}")
        throw CustomException(ex.getMessage)
    }
  }

}
