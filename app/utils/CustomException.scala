package utils

case class CustomException(message: String) extends Exception(message)
