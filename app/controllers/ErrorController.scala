package controllers

import javax.inject.{Inject, Singleton}

import play.api.mvc.{AbstractController, ControllerComponents}

@Singleton
class ErrorController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {

  def error = Action {
    Ok(views.html.error())
  }

}
