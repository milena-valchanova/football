package controllers

import javax.inject._
import connectors.GamesConnector
import models.Game
import org.joda.time.DateTime
import play.api.{Configuration, Logger}
import play.api.libs.ws.WSClient
import play.api.mvc._
import services.GamesService
import utils.CustomException
import scala.concurrent.ExecutionContext.Implicits.global

@Singleton
class GameController @Inject()(cc: ControllerComponents, ws: WSClient, config: Configuration) extends AbstractController(cc) {

  lazy val numberOfRecentGames = config.get[Int]("number.of.games.recent")
  lazy val numberOfUpcomingGames = config.get[Int]("number.of.games.upcoming")
  val gamesConnector: GamesConnector = new GamesConnector(ws, config)
  val gamesService: GamesService = new GamesService
  def now = DateTime.now
  val logger: Logger = Logger

  def index = Action.async {
    gamesConnector.getFootballEvents.map { res =>
      val gamesData: Seq[Game] = gamesService.parse(res)
      val recentGames: Seq[Game] = gamesService.extractGames(gamesData, numberOfRecentGames, now, false)
      val upcomingGames: Seq[Game] = gamesService.extractGames(gamesData, numberOfUpcomingGames, now, true)
      Ok(views.html.games(recentGames, upcomingGames))
    }.recover {
      case cex: CustomException =>
        Redirect(routes.ErrorController.error)
      case ex: Exception =>
        logger.error(s"Exception in HomeController: ${ex.getMessage}")
        Redirect(routes.ErrorController.error)
    }
  }

}
